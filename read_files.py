##################################################################################
# read_files.py                                                                  #
#                                                                                #
# given a directory,                                                             #
# determine the file type, and                                                   #
# parse all the data into an array of time series.                               #
# (additionally, honor requests for lower/upper bounds on a specified variable.) #
#                                                                                #
##################################################################################

# return format:
# for each variable (one-per-timestep),
# write a file with each time series of that variable in a row.
# for each variable (one-per-timeseries),
# write a single-column file with each row corresponding to a time series.
# for each variable (several-per-timestep),


import numpy as np
import sys
np.set_printoptions(threshold=np.nan)
import h5py                                        
from os import listdir
from os.path import isfile, join
import pickle
import math

def main():
    #FIXME: needs to be specified via interface, and definitely not hard-coded
    #files_path = '/Users/annie/Documents/vidi labs research/data/merger_trees_d0/' # path on my laptop....
    files_path = '/Users/anniepreston/Documents/data/merger tree data/merger_trees_d0/' # path on work desktop...

    #FIXME: ALSO, any desired bounds should be specified via interface...

    #FIXME: note requirement: the 'files_path' should contain all the files that you want read, and no other files;
    # and, must all be of the same type!
    #DI, ms, NI, tags, times, vels, xyzvels, xyzpos = readfiles(files_path)
    data, variables = readfiles(files_path)

    # if these data contain a tree structure (how to check for this??), 
    # (could specify, like, 'tree?' --> 'descendent index name?')
    # or, check if there's a 'descIndex' or 'descendentIndex' or w/e field
    if 'descendentIndex' in variables:
        print('tree data identified!...')

        #FIXME: for now, automatically simplify trees to just return the main branch and the merger information, 
        # but would like to visualize entire trees
        flatTrees = getFlattenedTrees(data, variables)
        mergers = getAllMergers(data, variables)


    """
    getMainMasses(DI, ms, NI, tags, times, vels, mass_file, halo_tag_file, timestep_file, velocity_file)
    getMergerRatios(DI, ms, NI, merger_file)

    mass_npy_file = mass_file + '_normed.npy'
    vel_npy_file = velocity_file + '_normed.npy'
    sim_matrix = similarity(vel_npy_file, sim_file)
    links = linkage(sim_file + '.npy', linkage_file)

    #for Millennium:
    input_file = 'millennium/millennium_e.csv'
    mass_file = 'mill_e_mass_raw_NEW'
    timestep_file = 'mill_e_time_NEW'
    merger_file = 'mill_e_mergers_NEW'
    property_file = 'mill_e_properties_NEW'
    sim_file = 'mill_e_sim_NEW'
    linkage_file = 'mill_e_linkage_NEW'
    
    readMillFile(input_file, mass_file, timestep_file, merger_file, property_file)

    mass_npy_file = mass_file + '.npy'
    sim_matrix = similarity(mass_npy_file, sim_file)
    links = linkage(sim_file + '.npy', linkage_file)
    """

def readfiles(path): #FIXME: this function should take bounds for any variable
    filenames = [f for f in listdir(path) if isfile(join(path, f)) and f[0] != '.']
    print('reading files: ', filenames)

    #find out what type of files these are:
    if filenames[0][-4:] == 'hdf5':
        data, variables = readHDF5(path, filenames)
        return data, variables

    #FIXME: add option for NetCDF files

    #FIXME: add escape for files that don't fit either (any) of the accepted formats

def readHDF5(path, filenames):
    # create empty lists for the variable names and paths:
    allVariables = []
    allPaths = []

    # get the dataset names:
    with h5py.File(path + filenames[0], 'r') as hf:
        groupNames = list(hf.keys())
        for group in groupNames:
            variableNames = list(hf.get(group).keys())
            for variable in variableNames:
                allVariables.append(variable)
                allPaths.append(group + '/' + variable)

    # FIXME: at this point, show user the extracted fields and let them choose not to extract any of them, if not desired
    # ALSO, here, include anything that's tree-specific if necessary
    # FIXME: for now, hard-coding/removing per-tree data
    # ^^ and, 'descIndices' (and similar?) are required but shouldn't be included in the possible clustering features
    requestedIndices = [i for i in range(0,11)] # <-- this should be input from the interface

    variables = [v for ind, v in enumerate(allVariables) if ind in requestedIndices]
    paths = [p for ind, p in enumerate(allPaths) if ind in requestedIndices]

    # create a list of numpy arrays, one for each of the fields we're going to extract
    data = []

    first = True # so we know to make a new array only the first time
    for file in filenames:
        print('reading file: ', file)
        with h5py.File(path + file, 'r') as hf:
            i = 0
            for p in paths:
                print(p)
                values = hf.get(p)
                if first == True:
                    data.append(np.array(values))
                else:
                    data[i] = np.concatenate((data[i], np.array(values)))
                i += 1
            first = False

    return data, variables

def getFlattenedTrees(data, variables):
    # for each variable,
    # for each tree, return a time series
    # i.e., each item in the list corresponds to one variable, and contains the time series of every tree for that variable
    # FIXME: ^ expain this better.

    flatData = []
    for var in variables:
    	flatData.append([])

    diIndex = variables.index('descendentIndex') # index of descendent indices array in data
    descendentIndices = data[diIndex]
    
    treeIndex = 0 # number of trees processed so far
    for i in range(2000):# range(len(descendentIndices)):
    	print(i)
    	if descendentIndices[i] == -1:
    		# new tree:
    		treeIndex += 1
    		# start a new row for each variable
    		for v in range(len(variables)):
    			print(data[v][i])
    			flatData[v].append([data[v][i]])
    		print(flatData)
    	else:
    		for v in range(len(variables)):
    			flatData[v][treeIndex].append(data[v][i])
    print(flatData)
"""
    csvfilename = flatmassfile + '.csv'
    with open(csvfilename, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in mass:
            writer.writerow(item)

    csvfilename = flattimefile + '.csv'
    with open(csvfilename, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in time:
            writer.writerow(item)

    csvfilename = flatvelfile + '.csv'
    with open(csvfilename, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in vel:
            writer.writerow(item)

    csvfilename = flatxyzvelfile + '.csv'
    with open(csvfilename, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in xyzvels:
            writer.writerow(item)

    csvfilename = flatxyzposfile + '.csv'
    with open(csvfilename, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in xyzpos:
            writer.writerow(item)
"""
def getAllMergers(DI, mass, time, mergertimefile, mergerfile, mergermassfile):
    mergerTimes = []
    mergedMasses = []
    mergers = [] # 1.0 or above, depending on cumulative effect of progenitors for this halo
    for j in range(len(DI)): # for each tree:
        id_row = DI[j] # descendent indices for this tree
        mass_row = mass[j]
        time_row = time[j]

        merger_row = []
        merger_time_row = []
        merged_mass_row = []
        merger_dict = {} # index : list of progenitor masses-pl,

        for i in range(1, len(id_row)):
            if id_row[i] in merger_dict:
                merger_dict[id_row[i]].append(mass_row[i])
            else:
                merger_dict[id_row[i]] = [mass_row[i]]

        for id in list(merger_dict.keys()): # for each parent, determine the mass ratio and the timestep
            if len(merger_dict[id]) == 1:
                merger_ratio = 0
            else:
                progenitor_mass = sum(merger_dict[id])
                progenitor_mass = progenitor_mass - np.amax(np.asarray(merger_dict[id]))
                merger_ratio = progenitor_mass/mass_row[id] # merger ratio = m/M, where m = sum of progenitors that *aren't* the main branch and M = halo mass
            timestep = time_row[id]
            merger_row.append(merger_ratio)
            merger_time_row.append(timestep)
            merged_mass_row.append(mass_row[id])
        mergers.append(merger_row)
        mergerTimes.append(merger_time_row)
        mergedMasses.append(merged_mass_row)

    csvfilename = mergertimefile + '.csv'
    with open(csvfilename, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in mergerTimes:
            writer.writerow(item)

    csvfilename = mergerfile + '.csv'
    with open(csvfilename, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in mergers:
            writer.writerow(item)

    csvfilename = mergermassfile + '.csv'
    with open(csvfilename, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in mergedMasses:
            writer.writerow(item)

def getPairedTrees(DI, mass, vel, time, pairedmassfile, pairedtimefile, pairedvelfile):
    pairedMasses = []
    pairedTimes = []
    pairedVels = []
    for j in range(len(DI)):
        row = DI[j]
        mass_row = mass[j]
        time_row = time[j]
        vel_row = vel[j]
        treeMass = []
        treeTime = []
        treeVel = []
        
        for i in range(1, len(row)):
            treeMass.append(mass_row[row[i]])
            treeMass.append(mass_row[i])
            treeVel.append(vel_row[row[i]])
            treeVel.append(vel_row[i])
            treeTime.append(time_row[row[i]])
            treeTime.append(time_row[i])
        pairedMasses.append(treeMass)
        pairedTimes.append(treeTime)
        pairedVels.append(treeVel)

    for i in range(len(pairedMasses[0])):
        print(pairedMasses[0][i], ", ", pairedTimes[0][i])


    csvfilename = pairedmassfile + '.csv'
    with open(csvfilename, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in pairedMasses:
            writer.writerow(item)

    csvfilename = pairedtimefile + '.csv'
    with open(csvfilename, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in pairedTimes:
            writer.writerow(item)

    csvfilename = pairedvelfile + '.csv'
    with open(csvfilename, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in pairedVels:
            writer.writerow(item)

#read millennium-derived file and save scaled mass lists
#for now: assume these are already binned
#(NOTE: this bypasses a step that the HDF HACC data must go through!)
def readMillFile(infile, timeseriesfile, timestepfile, mergerfile, propertyfile, minlength=10):
    
    massLists = []
    timeLists = []
    mergerLists = []
    propLists = []
    rawData = []
    
    with open(infile, 'r') as csvfile:
        millReader = csv.reader(csvfile, delimiter=',')
        for row in millReader:
            insert_row = []
            for item in row:
                insert_row.append(float(item))
            rawData.append(insert_row)
    row = []
    timerow = []
    mergerrow = [] #to store merger ratios
    proprow = [] #to store halo/galaxy properties for final timestep
    for i in range(len(rawData)):
        if i == 0:
            mergerrow.append(0.0)
            row.append(rawData[i][1])
            proprow.append(rawData[i][2:])
            propLists.append(proprloow)
            proprow = []
            timerow.append(rawData[i][0])
        if rawData[i][0] < rawData[i-1][0] and i != 0:
            row.append(rawData[i][1])
            timerow.append(rawData[i][0])
            if rawData[i][1] != 0:
                mergerrow.append(float(rawData[i-1][1]/rawData[i][1]))
            else: mergerrow.append(0)
        if rawData[i][0] > rawData[i-1][0] and i != 0:
            proprow.append(rawData[i][2:])
            if len(row) > minlength:
                propLists.append(proprow)
                massLists.append(row)
                timeloaLists.append(timerow)
                mergerLists.append(mergerrow)
            mergerrow = []
            proprow = []
            timerow = []
            row = []
            row.append(rawData[i][1])
            timerow.append(rawData[i][0])
            mergerrow.append(0.0)
    if len(row) > minlength:
        massLists.append(row)
        mergerLists.append(mergerrow)
        timeLists.append(timerow)

    #create uniform lengths
    for i in range(len(massLists)):
        while len(massLists[i]) < 100:
            massLists[i].append(-99)
            mergerLists[i].append(0.0)
            timeLists[i].append(-99)

    #write unnormalized data to CSV files for visualization program
    csvfilename = timeseriesfile + '.csv'
    with open(csvfilename, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in massLists:
            writer.writerow(item)

    csvfilename = timestepfile + '.csv'
    with open(csvfilename, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in timeLists:
            writer.writerow(item)

    csvfilename = mergerfile + '.csv'
    with open(csvfilename, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in mergerLists:
            writer.writerow(item)
                                 
    csvfilename = propertyfile + '.csv'
    with open(csvfilename, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in propLists:
            writer.writerow(item)
                 
    #normalize mass data
    for i in range(len(massLists)):
        max_mass = max(massLists[i])
        if max_mass > 0:
            massLists[i] = [float(val)/max_mass for val in massLists[i]]
            print("normalized: ", massLists[i])

    #write to npy file for similarity function
    np.save(timeseriesfile, massLists)

########################

# obtain main mass representation of the data
# (for HACC data)

#utility function:
def findMainMass(descIndices, masses, nodeIndices, tags, times, vels, node):
    candidate_masses = []
    candidate_nodes = []
    candidate_tags = []
    candidate_times = []
    candidate_vels = []
    desc_found = 0
    for i in range(len(descIndices)):
        if descIndices[i] == node:
            desc_found = 1
            candidate_masses.append(masses[i])
            candidate_nodes.append(nodeIndices[i])
            candidate_tags.append(tags[i])
            candidate_times.append(times[i])
            candidate_vels.append(vels[i])

    if desc_found == 1:
        maxMass = max(candidate_masses)
        maxIndex = candidate_masses.index(maxMass)
        return candidate_nodes[maxIndex], candidate_masses[maxIndex], candidate_tags[maxIndex], candidate_times[maxIndex], candidate_vels[maxIndex]
    if desc_found == 0:
        return -99, -99, -99, -99, -99

def getMainMasses(dI, ms, nI, in_tags, in_times, in_vels, outfile, tagfile, timefile, velfile):
    print('getting main masses...')
    mainMassLists = []
    tagLists = []
    timeLists = []
    velLists = []
    j = 0
    normalize = 0
    for i in range(len(dI)):
        if len(dI[i]) > 1:     # if the tree is more than just a single halo
            newMassList = []
            newTagList = []
            newVelList = []
            newTimeList = []
            mainMassLists.append(newMassList)
            tagLists.append(newTagList)
            timeLists.append(newTimeList)
            velLists.append(newVelList)
            
            node = nI[i][0]
            descIndices = [num for num in dI[i] if num != -99]
            nodeIndices = [num for num in nI[i] if num != -99]
            masses = [num for num in ms[i] if num != -99]
            tags = [num for num in in_tags[i] if num != -99]
            times = [num for num in in_times[i] if num != -99]
            vels = [num for num in in_vels[i] if num != -99]
            
            while node != -99:
                node, mass, tag, time, vel = findMainMass(descIndices, masses, nodeIndices, tags, times, vels, node)
                if node != -99:
                    mainMassLists[j].append(mass)
                    tagLists[j].append(tag)
                    timeLists[j].append(time)
                    velLists[j].append(vel)
            #normalize?
            if normalize == 1:
                max_mass = max(mainMassLists[j])
                if max_mass > 0:
                    mainMassLists[j] = [float(num)/max_mass for num in mainMassLists[j]]
            #uniform length: (FIXME: is this needed?)
            while len(mainMassLists[j]) < 100:
                mainMassLists[j].append(-99)
            j += 1

    np.save(outfile, mainMassLists)
    np.save(tagfile, tagLists)
    np.save(timefile, timeLists)
    np.save(velfile, velLists)

    csvoutfile = outfile + '.csv'
    csvtagfile = tagfile + '.csv'
    csvtimefile = timefile + '.csv'
    csvvelfile = velfile + '.csv'
    
    with open(csvoutfile, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in mainMassLists:
                writer.writerow(item)
    with open(csvtagfile, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in tagLists:
            writer.writerow(item)
    with open(csvtimefile, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in timeLists:
            writer.writerow(item)
    with open(csvvelfile, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in velLists:
            writer.writerow(item)

    #normalize mass data
    for i in range(len(mainMassLists)):
        max_mass = max(mainMassLists[i])
        if max_mass > 0:
            mainMassLists[i] = [float(val)/max_mass for val in mainMassLists[i]]

    #normalize vel data
    for i in range(len(velLists)):
        max_vel = max(velLists[i]);
        if max_vel > 0:
            velLists[i] = [float(val)/max_vel for val in velLists[i]]

    #write to npy file for similarity function
    np.save(outfile + '_normed', mainMassLists)
    np.save(velfile + '_normed', velLists)

#########################

# obtain merger ratio representation of data
# (for HACC data)

#utility function:
def findMergerRatio(descIndices, masses, nodeIndices, node):
    desc_found = 0
    ratio = 0.0
    prog_masses = []
    prog_nodes = []
    for i in range(len(descIndices)):
        if descIndices[i] == node:
            desc_found += 1
            prog_masses.append(masses[i])
            prog_nodes.append(nodeIndices[i])
    if desc_found > 1:
        maxMass = max(prog_masses)
        maxIndex = prog_masses.index(maxMass)
        ratio = (sum(prog_masses) - maxMass)/maxMass
        return prog_nodes[maxIndex], ratio
    if desc_found == 1:
        return prog_nodes[0], 0.0
    if desc_found == 0:
        return -99, -99

def getMergerRatios(dI, ms, nI, outfile):
    print('getting merger ratios...')
    mergerRatioLists = []
    j = 0
    for i in range(len(dI)):
        if len(dI[i]) > 1:   # if the tree is more than just a single halo
            newList = []
            mergerRatioLists.append(newList)
            node = nI[i][0]
            descIndices = dI[i]
            nodeIndices = nI[i]
            masses = ms[i]
            
            while node != -99:
                node, ratio = findMergerRatio(descIndices, masses, nodeIndices, node)
                if node != -99:
                    mergerRatioLists[j].append(ratio)
            j += 1
    np.save(outfile, mergerRatioLists)
    csvoutfile = outfile + '.csv'
    with open(csvoutfile, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for item in mergerRatioLists:
            writer.writerow(item)
main()
