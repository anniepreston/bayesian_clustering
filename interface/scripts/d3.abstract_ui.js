var width = window.innerWidth;
var height = window.innerHeight;

var dataManager = new DataManager("datamanager");
EventBus.addEventListener("slider_event", dataManager.updateNClusters, dataManager);
EventBus.addEventListener("timeseries_event", dataManager.updateTimeseries, dataManager);
EventBus.addEventListener("linkage_event", dataManager.updateLinkage, dataManager);
EventBus.addEventListener("view_event", dataManager.updateView, dataManager);
EventBus.addEventListener("encoding_event", dataManager.updateEncoding, dataManager);
EventBus.addEventListener("dash_event", dataManager.updateDash, dataManager);

//var clusterView = new ClusterView("clusters");
//var phaseplotView = new PhaseplotView("phases");