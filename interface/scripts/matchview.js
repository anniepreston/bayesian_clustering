import React, { Component } from 'react';
import ReactDOM, {render} from 'react-dom';
import {connect} from 'react-redux';
import * as d3 from 'd3';

const defaultProps = {
	highlightColor: '#458af9',
	averageColor: '#57af75',
	worstColor: '#f25532'
}

const height = window.innerHeight; //FIXME

export class MatchView extends Component{
	constructor(props){
		super(props);
        this.state = {
            data: {},
            maxes: [],
            mins: [],
            nClusters: 1, //FIXME!
            height: 800,
            width: 800
        }
		this.drawMatches = this.drawMatches.bind(this);
		/*
		this.drawAxes = this.drawAxes.bind(this);
		this.drawTimeSeries = this.drawTimeSeries.bind(this);
		*/
	}
	componentDidMount(){
		if (document.getElementsByClassName('lm_item').length > 0){
			this.setState({width: parseFloat(document.getElementsByClassName('lm_item')[2].style.width.slice(0,-2))}); //FIXME
			this.setState({height: parseFloat(document.getElementsByClassName('lm_item')[2].style.height.slice(0,-2))}); //FIXME
		}
        window.addEventListener('resize', this._resize.bind(this));
        this._resize();
	}

	componentDidUpdate(){
		this.drawMatches();
	}

	_resize(){
		if (document.getElementsByClassName('lm_item').length > 0){
			this.setState({width: parseFloat(document.getElementsByClassName('lm_item')[2].style.width.slice(0,-2))}); //FIXME
			this.setState({height: parseFloat(document.getElementsByClassName('lm_item')[2].style.height.slice(0,-2))}); //FIXME
		}
		this.drawMatches();
		//this.drawAxes();
	}

	drawMatches(){
		const node = this.node;
		d3.select(node).selectAll("*").remove();
		const { matchSettings, data, nClusters, linkage } = this.props;
		if (typeof data !== 'undefined' && '1' in data){
			const timeSeries = data[nClusters][linkage].timeSeries;
			const sim = data[nClusters][linkage].similarity;
			const idx = matchSettings.idx;

			//get average similarity:
			/*
			var sum = 0,
				n = 0;
			console.log("sim: ", sim);
			for (var i = 0; i < sim[idx].length; i++){
				n += 1;
				sum += sim[idx][i];
			}
            var averageSimButton = d3.select(node).append('g')
            	.attr("class", "label")
            	.attr("transform", "translate(28,100)")
         
            averageSimButton.append("text")
				.attr("x", 0)
				.attr("dy", ".31em")
				.attr('font-family', 'sans-serif')
				.attr("font-size", "14px")

            var average_similarity = sum/n;

            averageSimButton.select("text").text("average similarity: " + average_similarity);
            */
            //normalize here, if doing...
            /*
    		for (var i = 0; i < timeSeries.length; i++){
		        max = np.amax(series[i])

    		}
        	for j in range(len(series[i])):
            	series[i][j] = series[i][j]/max
            */

    		//find the worst similarity:
    		/*
		    var largest_small = 0;
    		var small_i = 0;
    		var small_j = 0;
    		for (var i = 0; i < sim.length; i++){
        		var smallest = 10000;
        		for (var j = 0; j < sim[0].length; j++){
        			if (sim[i][j] < smallest && sim[i][j] > 0){
        				smallest = sim[i][j];
        			}
            	}	
    			if (smallest > largest_small){
            		largest_small = smallest;
            		small_i = i;
            	}
    		} 
    		*/
    		console.log("A...");
    		var similarity = sim[idx]
			//var sorted_sim = similarity.sort();
			var sorted = this.sortWithIndices(similarity);
			var sorted_sim = sorted.array;
			var sorted_ids = sorted.sortIndices;
			var last_idx = sim.length-1;
			console.log("B...");

    		var sims = [sorted_sim[0], sorted_sim[1], sorted_sim[2], 
    			sorted_sim[Math.floor(last_idx/2)], sorted_sim[Math.floor(last_idx/2 + 1)], 
    			sorted_sim[last_idx-2], sorted_sim[last_idx-1], sorted_sim[last_idx]]
    		var ids = [idx, sorted_ids[1], sorted_ids[2], 
    			sorted_ids[Math.floor(last_idx/2)], sorted_ids[Math.floor(last_idx/2 + 1)], 
    			sorted_ids[last_idx-2], sorted_ids[last_idx-1], sorted_ids[last_idx]]
    		var colors = [defaultProps.highlightColor, defaultProps.highlightColor, defaultProps.highlightColor,
    			defaultProps.averageColor, defaultProps.averageColor,
    			defaultProps.worstColor, defaultProps.worstColor, defaultProps.worstColor];

    		const {width, height} = this.state;
    		const currentVar = 'mainMass';
    		console.log("C...");

    		//var { xRange, yRange } = this.state;
    		var xRange = {};
    		var lineData = [];
	    	const yRange = this.getYRange(timeSeries, currentVar, ids);
	        for (var i = 0; i < ids.length; i++){
	            var array = timeSeries[currentVar][ids[i]];
	            var timeArray = timeSeries['mainTime'][ids[i]];
	            var data_row = [];
	            xRange.min = 40; //FIXME!
	            xRange.max = timeArray[timeArray.length-1]; //FIXME!
	            for (var t_idx = 0; t_idx < timeArray.length; t_idx++){
	            	var t = timeArray[t_idx];
	                if (array[t] != -Infinity && array[t] != Infinity){
	                    var entry = {};
	                    entry.x = this.xScale(t, xRange);
	                    entry.y = height - (this.yScale(array[t_idx], yRange) + 60);
	                    data_row.push(entry);
	                }
	            }
	            lineData.push(data_row);
	        }
	        console.log("D...");
		    var line = d3.line()
		        .x( d => d.x )
		        .y( d => d.y );
		    
	        d3.select(node).selectAll(".line")
		        .data(lineData)
		        .enter().append("path")
		        .style("stroke", function(d,i){ 
		            return colors[i]; })
		        .style("fill", "none")
		        .style("stroke-width", function(d,i){
		        	if (i == 0) return "4px";
		        	else return "2px";
		        })
		        .attr("class", "line")
		        .attr("d", line);

			const legend = [{
				name: 'nearest', color: '#458af9'
			}, {
				name: 'average', color: '#57af75'
			}, {
				name: 'farthest', color: '#f25532'
			}];

		    var legend = d3.select(node).selectAll(".rect")
		    	.data(legend)
		    	.enter()
		    	.append("rect")
		    	.attr("x", 8)
		    	.attr("y", function(d,i){
		    		return 8 + 22*i;
		    	})
		    	.attr("width", 30)
		    	.attr("height", 20)
		    	.attr("fill", function(d,i){
		    		return d.color;
		    	});
		    var legendText = d3.select(node).append("g")
		    	.attr("class", "label");

		    legendText.selectAll("g")
		    	.data(legend)
		    	.enter().append("text")
                .text(function(d) { return d.name; })
                .attr("font-family", "sans-serif")
                .attr("font-size", "12px")
                .attr("transform", function(d,i){
                	return "translate(" + 40 + "," + (22 + 22*i) + ")";
                });
    	}
    }
    xScale(x, xRange){
    	const { width } = this.state;
    	var w = width;
        return (x-xRange.min)/(xRange.max-xRange.min)*(0.95*w); //FIXME
    }
    yScale(y, yRange){
    	const { height, width } = this.state;
    	const { nClusters } = this.props;
        var frac = (y - yRange.min)/(yRange.max - yRange.min);
        if (y > yRange.max) frac = 1.0; 
        var clusterHeight = 0.9*(height)/nClusters;
        return frac*clusterHeight;
    }
    getYRange(timeSeries, varName, ids){
        var { maxes, mins } = this.state;
        for (var i = 0; i < ids.length; i++){ 
        	var array = timeSeries[varName][ids[i]];
            var arrayMax = Math.max.apply(Math, array);
            var arrayMin = Math.min.apply(Math, array);
            maxes.push(arrayMax);
            mins.push(arrayMin);
        }
        var range = {};
        range.min = Math.min.apply(Math, mins);
        range.max = Math.max.apply(Math, maxes);
        return range;
    }

    sortWithIndices(array) {
		for (var i = 0; i < array.length; i++) {
	    	array[i] = [array[i], i];
	  	}
  		array.sort(function(left, right) {
    		return left[0] < right[0] ? -1 : 1;
  		});
  		var data = {
  			array: [],
  			sortIndices: [],
  		}
		for (var j = 0; j < array.length; j++) {
		    data.sortIndices.push(array[j][1]);
		    data.array.push(array[j][0]);
		}
  		return data;
	}

	render(){
		const {width, height} = this.state;
        return <svg height={height} width={width} ref={node => this.node = node}>
        </svg>
        //
	}
}

const mapStateToProps = state => ({
    data: state.items,
    loading: state.loading,
    error: state.error,
    matchSettings: state.matchSettings,
    nClusters: state.nClusters,
    linkage: state.linkage
});

const MatchViewContainer = connect(mapStateToProps)(MatchView);

export default MatchViewContainer;

