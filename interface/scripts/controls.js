import React, { Component } from 'react';
import ReactDOM, {render} from 'react-dom';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {updateNClusters, updateView, updateMatchIdx, updateLinkage} from './dataActions';
import * as d3 from 'd3';

/*
  views: {
    velocity_on: 0.0,
    main_branches_on: 0.0,
    mergers_on: 0.0,
    merger_demo_on: 0.0
  }
  */

var viewOptions = [];   

var viewOptionsNames = ['velocity_on', 'main_branches_on', 'mergers_on', 'merger_demo_on'];
for (var i = 0; i < viewOptionsNames.length; i++){
    viewOptions.push({
        name: viewOptionsNames[i],
        state: 0
    });
}

var linkageOptions = [{
    name: 'complete', 
    state: 1
},{
    name: 'single',
    state: 0
}];

export class Controls extends Component {
    constructor(props){
        super(props);
        this.state = {
            nClusters: 1 //initial value
        };
        this.drawControls = this.drawControls.bind(this);
    }

    componentDidMount(){
        if (document.getElementsByClassName('lm_item').length > 0){
            this.setState({width: parseFloat(document.getElementsByClassName('lm_item')[3].style.width.slice(0,-2))}); //FIXME
            this.setState({height: parseFloat(document.getElementsByClassName('lm_item')[3].style.height.slice(0,-2))}); //FIXME
        }
        window.addEventListener('resize', this._resize.bind(this));
        this._resize();
    }

    componentDidUpdate(){
        this.drawControls();
    }
        
    _resize(){
        if (document.getElementsByClassName('lm_item').length > 0){
            this.setState({width: parseFloat(document.getElementsByClassName('lm_item')[3].style.width.slice(0,-2))}); //FIXME
            this.setState({height: parseFloat(document.getElementsByClassName('lm_item')[3].style.height.slice(0,-2))}); //FIXME
        }
        this._onViewportChange({
            width: window.innerWidth,
            height: window.innerHeight
        });
    }
    _onViewportChange(viewport) {
        this.setState({
            viewport: {...this.state.viewport, ...viewport}
        });
    }

    drawControls(){
        const node = this.node;
        const { width, height, nClusters } = this.state;
        const props = this.props;
        const { similarity, linkage } = this.props;
        if (width > 0){
            var nClusters_scale = d3.scaleLinear()
                .domain([1, 8])
                .range([0.2*width, 0.8*width])
                .clamp(true);

            d3.select(node).selectAll("g").remove();

            var slider = d3.select(node).append("g")
                .attr("class", "slider")
                .attr("transform", "translate(" + 0 + "," + 40 + ")")
                .call(d3.drag() 
                    .on("start.interrupt", function() {})
                    .on("start drag", function() { reset(nClusters_scale.invert(d3.event.x)); })
                    .on("end", function() { updateClusters(nClusters_scale.invert(d3.event.x)); }));

            slider.append("line")
                .attr("class", "track")
                .attr("x1", nClusters_scale.range()[0])
                .attr("x2", nClusters_scale.range()[1])
              .select(function() { return this.parentNode.appendChild(this.cloneNode(true)); })
                .attr("class", "track-inset")
              .select(function() { return this.parentNode.appendChild(this.cloneNode(true)); })
                .attr("class", "track-overlay");

            slider.insert("g", ".track-overlay")
                .attr("class", "ticks")
                .attr("transform", "translate(0," + 18 + ")")
              .selectAll("text")
              .data(nClusters_scale.ticks(4))
              .enter().append("text")
                .attr("x", nClusters_scale)
                .attr("text-anchor", "middle")
                .text(function(d) { return d; });

            var handle = slider.insert("circle", ".track-overlay")
                .attr("class", "handle")
                .attr("r", 9)
                .attr("cx", nClusters_scale(nClusters));
            
            slider.append("text")
                .attr("class", "title")
                .text(function(){ var text; if (nClusters == 1) text = "1 cluster"; else text = nClusters + " clusters"; return text; })
                .attr("transform", "translate(60,-14)")
                .attr("font-family", "sans-serif")
                .attr("font-size", "12px");

            // ~~~ VIEW OPTIONS ~~~ //

            var viewBox = d3.select(node).append("g")
                .attr("class", "checkbox")
                .attr("transform", "translate(" + 10 + "," + 120 + ")");
                
            viewBox.selectAll("g")
                .data(viewOptions)
                .enter()
                .append("rect")
                .attr("class", "optionborder")
                .attr("fill", function(d){ if (d.state == 0) return '#dddddd'; else return '#666666';})
                .attr("width", 16)
                .attr("height", 16)
                .attr("y", function(d,i){ return i*20; })
                .attr("x", 0)
                .attr("rx", 4)
                .attr("ry", 4)
                .on("click", function(d,i){ 
                    if (viewOptions[i].state == 1){
                        d3.select(this).style("fill", '#dddddd');
                    }
                    else {
                        d3.select(this).style("fill", '#666666');
                    }
                    if (viewOptions[i].state == 1){
                        viewOptions[i].state = 0;
                    }
                    else {
                        viewOptions[i].state = 1;
                    }
                    updateView(viewOptions);
                });
            
            var viewOptionsNames = ['velocity', 'main branches', 'mergers', 'mergers (all)'];
            var optionsText = d3.select(node).append("g")
                .attr("class", "checkbox")
                .attr("transform", "translate(" + 10 + "," + 120 + ")");

            optionsText.selectAll("g")
                .data(viewOptionsNames)
                .enter().append("text")
                .text(function(d) { return d; })
                .attr("font-family", "sans-serif")
                .attr("font-size", "12px")
                .attr("transform", function(d,i){ return "translate(" + 24 + "," + (12 + i*20) + ")"; });
                
                /*
            function toggleView(){
                var currentX = viewOption + 1;
                viewOption = currentX % 3;
                viewSelection.attr("x", viewOption*16)
                viewOptionText.text(viewOptions[viewOption]);
                
                dispatchView(viewOptions[viewOption], this.currentN);
            }
            */

            // ~~~ LINKAGE OPTIONS ~~~ //
            var linkageBox = d3.select(node).append("g")
                .attr("class", "checkbox")
                .attr("transform", "translate(" + 10 + "," + 220 + ")");
                
            linkageBox.selectAll("g")
                .data(linkageOptions)
                .enter()
                .append("rect")
                .attr("class", "linkageBorder")
                .attr("fill", function(d,i){ 
                    if (d.state == 0) return '#dddddd'; 
                    else return '#666666';
                })
                .attr("width", 16)
                .attr("height", 16)
                .attr("y", function(d,i){ return i*20; })
                .attr("x", 0)
                .attr("rx", 4)
                .attr("ry", 4)
                .on("click", function(d,i){ 
                    if (linkageOptions[i].state == 1){
                        d3.select(this).style("fill", '#dddddd');
                    }
                    else {
                        d3.select(this).style("fill", '#666666');
                    }
                    if (linkageOptions[i].state == 1){
                        linkageOptions[i].state = 0;
                    }
                    else {
                        linkageOptions[i].state = 1;
                    }
                    updateLinkage(linkageOptions);
                });
            
            var linkageOptionsNames = ['complete', 'single'];
            var linkageOptionsText = d3.select(node).append("g")
                .attr("class", "checkbox")
                .attr("transform", "translate(" + 10 + "," + 220 + ")");

            linkageOptionsText.selectAll("g")
                .data(linkageOptionsNames)
                .enter().append("text")
                .text(function(d) { return d; })
                .attr("font-family", "sans-serif")
                .attr("font-size", "12px")
                .attr("transform", function(d,i){ return "translate(" + 24 + "," + (12 + i*20) + ")"; });
                

            // ................. //

            var max_idx = 538; //FIXME!!!!!! this is a deadline artifact!

            var matchIdxButton = d3.select(node).append("g")
                .attr("class", "checkbox")
                .attr("transform", "translate(" + 10 + "," + 320 + ")")
                .append("rect")
                .attr("class", "optionborder")
                .attr("fill", '#dddddd')
                .attr("width", 16)
                .attr("height", 16)
                .attr("y", 0)
                .attr("x", 0)
                .attr("rx", 4)
                .attr("ry", 4)
                .on("mouseover", function(){
                    d3.select(this).style("fill", '#666666');
                })
                .on("mouseout", function(){
                    d3.select(this).style("fill", "#dddddd");
                })
                .on("click", function(){
                    d3.select(this).style("fill", "black");
                    updateMatchIdx(max_idx);
                });
            d3.select(node).append("g")
                .attr("transform", "translate(28,330)")
                .append("text")
                .attr("font-family", "sans-serif")
                .attr("font-size", "12px")
                .text("random tree");

            function reset(h) {
                handle.attr("cx", nClusters_scale(h));
            }
            
            function updateClusters(h){
                var nClusters = Math.round(h);
                handle.attr("cx", nClusters_scale(Math.round(h)));
                //dispatchNClusters(Math.round(h));
                d3.selectAll(".title")
                    .text(nClusters + " clusters");
                props.updateNClusters(nClusters);
            }

            function updateView(viewOptions){
                var views = {
                    'velocity_on': viewOptions[0].state,
                    'main_branches_on': viewOptions[1].state,
                    'mergers_on': viewOptions[2].state,
                    'merger_demo_on': viewOptions[3].state
                }
                props.updateView(views);
            }

            function updateLinkage(linkageOptions){
                var l;
                if (linkageOptions[0].state == 1){
                    l = 'complete';
                }
                else l = 'single';
                props.updateLinkage(l);
            }

            function updateMatchIdx(n){
                var i = Math.floor((Math.random() * n));
                props.updateMatchIdx(i);
            }
        }
        
        /*
        dispatchTimeseries = function(option) {
            EventBus.dispatch("timeseries_event", this, option);
        }
        dispatchLinkage = function(option) {
        	EventBus.dispatch("linkage_event", this, option, n);
        }
        dispatchView = function(option) {
        	EventBus.dispatch("view_event", this, option);
        }
        dispatchEncoding = function(option) {
        	EventBus.dispatch("encoding_event", this, option);
        }	
        dispatchDash = function(option, n) {
        	EventBus.dispatch("dash_event", this, option, n);
        }
        */
    	
    	//FIXME: consolidate these boxes !!
    	/*
    	var timeseriesOptions = ['none', 'random', 'all']
    	var currentOption = 0;
    	
    	var timeseriesBox = svg.append("g")
    	    .attr("class", "checkbox")
    	    .attr("transform", "translate(" + 0.4*this.width + "," + (this.height/2 - 8) + ")")
    	    .call(d3.drag()
                .on("start drag", function() { toggleTimeSeries(); })
            );
    	    
    	timeseriesBox.append("rect")
    		.attr("class", "optionborder")
    	    .attr("width", 3*16)
    	    .attr("height", 16)
    	    .attr("x", 0)
    	    .attr("y", 0)
            .attr("rx", 4)
            .attr("ry", 4);
            
        var timeseriesSelection = timeseriesBox.append("rect")
    		.attr("class", "optionbox")
            .attr("width", 16)
            .attr("height", 16)
            .attr("rx", 4)
            .attr("ry", 4);
            
        timeseriesBox.append("text")
            .text("show time series:")
            .attr("transform", "translate(0,-6)")
            .attr("font-family", "sans-serif")
            .attr("font-size", "12px");
        
        var timeseriesOptionText = timeseriesBox.append("text")
            .text(timeseriesOptions[currentOption])
            .attr("font-family", "sans-serif")
            .attr("font-size", "10px")
            .attr("transform", "translate(" + 0 + "," + "26)");

        function toggleTimeSeries(){
            var currentX = currentOption + 1;
            currentOption = currentX % 3;
            console.log("new x: ", currentOption*16);
            timeseriesSelection.attr("x", currentOption*16)
            timeseriesOptionText.text(timeseriesOptions[currentOption]);
            
            dispatchTimeseries(timeseriesOptions[currentOption]);
        }
        */
        /*    
        var linkageData = ['single', 'average', 'complete']
        var linkageOption = 2;
    	var linkageBox = svg.append("g")
    	    .attr("class", "checkbox")
    	    .attr("transform", "translate(" + 0.5*this.width + "," + (this.height/2 - 8) + ")")
    	    .call(d3.drag()
                .on("start drag", function() { toggleLinkage(); })
            );
    	    
    	linkageBox.append("rect")
    		.attr("class", "optionborder")
    	    .attr("width", 3*16)
    	    .attr("height", 16)
    	    .attr("x", 0)
    	    .attr("y", 0)
            .attr("rx", 4)
            .attr("ry", 4);
    	    
    	var linkageSelection = linkageBox.append("rect")
    		.attr("class", "optionbox")
            .attr("width", 16)
            .attr("height", 16)
            .attr("x", linkageOption*16)
            .attr("rx", 4)
            .attr("ry", 4);
            
        linkageBox.append("text")
            .text("linkage:")
            .attr("transform", "translate(0,-6)")
            .attr("font-family", "sans-serif")
            .attr("font-size", "12px");
        
        var linkageOptionText = linkageBox.append("text")
            .text(linkageData[linkageOption])
            .attr("font-family", "sans-serif")
            .attr("font-size", "10px")
            .attr("transform", "translate(" + 0 + "," + "26)");
            
        function toggleLinkage(){
            var currentX = linkageOption + 1;
            linkageOption = currentX % 3;
            linkageSelection.attr("x", linkageOption*16)
            linkageOptionText.text(linkageData[linkageOption]);
            
            dispatchLinkage(linkageData[linkageOption], this.currentN);
        }
    
        */
        /*
        var encodingOptions = ['flat', 'bands', 'color']
        var encodingOption = 0;
    	var encodingBox = svg.append("g")
    	    .attr("class", "checkbox")
    	    .attr("transform", "translate(" + 0.6 * this.width + "," + (this.height/2 - 8) + ")")
    	    .call(d3.drag()
                .on("start drag", function() { toggleEncoding(); })
            );
    	    
    	encodingBox.append("rect")
    		.attr("class", "optionborder")
    	    .attr("width", 3*16)
    	    .attr("height", 16)
    	    .attr("x", 0)
    	    .attr("y", 0)
            .attr("rx", 4)
            .attr("ry", 4);
    	    
    	var encodingSelection = encodingBox.append("rect")
    		.attr("class", "optionbox")
            .attr("width", 16)
            .attr("height", 16)
            .attr("rx", 4)
            .attr("ry", 4);
            
        encodingBox.append("text")
            .text("colors:")
            .attr("transform", "translate(0,-6)")
            .attr("font-family", "sans-serif")
            .attr("font-size", "12px");
                
        var encodingOptionText = encodingBox.append("text")
            .text(encodingOptions[encodingOption])
            .attr("font-family", "sans-serif")
            .attr("font-size", "10px")
            .attr("transform", "translate(" + 0 + "," + "26)");
            
        function toggleEncoding(){
            var currentX = encodingOption + 1;
            encodingOption = currentX % 3;
            encodingSelection.attr("x", encodingOption*16)
            encodingOptionText.text(encodingOptions[encodingOption]);
            
            dispatchEncoding(encodingOptions[encodingOption], this.currentN);
        }
        */
        /*
        var dashOptions = ['none', 'pies', 'maps', 'space']
        var dashOption = 0;
    	var dashBox = svg.append("g")
    	    .attr("class", "checkbox")
    	    .attr("transform", "translate(" + 0.9 * this.width + "," + (this.height/2 - 8) + ")")
    	    .call(d3.drag()
                .on("start drag", function() { toggleDash(); })
            );
    	    
    	dashBox.append("rect")
    		.attr("class", "optionborder")
    	    .attr("width", 4*16)
    	    .attr("height", 16)
    	    .attr("x", 0)
    	    .attr("y", 0)
            .attr("rx", 4)
            .attr("ry", 4);
    	    
    	var dashSelection = dashBox.append("rect")
    		.attr("class", "optionbox")
            .attr("width", 16)
            .attr("height", 16)
            .attr("rx", 4)
            .attr("ry", 4);
            
        dashBox.append("text")
            .text("view:")
            .attr("transform", "translate(0,-6)")
            .attr("font-family", "sans-serif")
            .attr("font-size", "12px");
                
        var dashOptionText = dashBox.append("text")
            .text(dashOptions[dashOption])
            .attr("font-family", "sans-serif")
            .attr("font-size", "10px")
            .attr("transform", "translate(" + 0 + "," + "26)");
            
        function toggleDash(){
            var currentX = dashOption + 1;
            dashOption = currentX % 4;
            dashSelection.attr("x", dashOption*16)
            dashOptionText.text(dashOptions[dashOption]);
            
            dispatchDash(dashOptions[dashOption], n);
        }
        */
    }
    render(){
        const {width, height} = this.state;
        return <svg width={width} height={height} ref={node => this.node = node}>
        </svg>
    }
}

Controls.PropTypes = {
    updateNClusters: PropTypes.func.isRequired,
    updateView: PropTypes.func.isRequired,
    updateMatchIdx: PropTypes.func.isRequired,
    updateLinkage: PropTypes.func.isRequired
}	

function mapDispatchToProps(dispatch){
    return {
        updateNClusters: n => dispatch(updateNClusters(n)),
        updateView: views => dispatch(updateView(views)),
        updateMatchIdx: i => dispatch(updateMatchIdx(i)),
        updateLinkage: l => dispatch(updateLinkage(l))
    }
}

const ControlsContainer = connect(null, mapDispatchToProps)(Controls);

export default ControlsContainer;