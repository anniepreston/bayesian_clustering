import React, { Component } from 'react';
import ReactDOM, {render} from 'react-dom';
import {connect} from 'react-redux';
import {fetchData} from './dataActions';
import $ from 'jquery';
import 'jquery-csv';
import * as d3 from 'd3';

const defaultProps = {
	colors: ["#a6cee3","#1f78b4","#b2df8a","#33a02c","#fb9a99","#e31a1c","#fdbf6f","#ff7f00","#cab2d6","#6a3d9a","#ffff99","#b15928"]
}

const height = window.innerHeight; //FIXME

export class TreeView extends Component{
	constructor(props){
		super(props);
        this.state = {
            data: {},
            maxes: [],
            mins: [],
            nClusters: 1, //FIXME!
            height: 800,
            width: 800
        }
		/*  views: {
		    velocity_on: 0.0,
		    main_branches_on: 0.0,
		    mergers_on: 0.0,
		    merger_demo_on: 0.0
		  }
		  */
		this.createTreeView = this.createTreeView.bind(this);
		this.drawAxes = this.drawAxes.bind(this);
		this.drawTimeSeries = this.drawTimeSeries.bind(this);
	}

	componentDidMount(){
		if (document.getElementsByClassName('lm_item').length > 0){
			this.setState({width: parseFloat(document.getElementsByClassName('lm_item')[2].style.width.slice(0,-2))}); //FIXME
			this.setState({height: parseFloat(document.getElementsByClassName('lm_item')[2].style.height.slice(0,-2))}); //FIXME
		}
        this.props.dispatch(fetchData());
        //this.drawAxes();
        window.addEventListener('resize', this._resize.bind(this));
        this._resize();
	}

	componentDidUpdate(){
		this.createTreeView();
		this.drawTimeSeries();
		//this.drawAxes();
	}

	componentWillUpdate(){
		this.createTreeView();
		this.drawTimeSeries();
		//this.drawAxes();
	}

	_resize(){
		if (document.getElementsByClassName('lm_item').length > 0){
			this.setState({width: parseFloat(document.getElementsByClassName('lm_item')[2].style.width.slice(0,-2))}); //FIXME
			this.setState({height: parseFloat(document.getElementsByClassName('lm_item')[2].style.height.slice(0,-2))}); //FIXME
		}
		this.createTreeView();
		this.drawTimeSeries();
		//this.drawAxes();
		this._onViewportChange({
			//width: width,
			//height: height
		});
	}

	_onViewportChange(viewport) {
    	this.setState({
      		viewport: {...this.state.viewport, ...viewport}
    	});
  	}

	createTreeView(){
    	//(dataStats, encodingOption)
    	//array of arrays (one per cluster/ribbon): [{x: , y: , t: }]
    	const node = this.node;
    	d3.select(node).selectAll("*").remove();

    	var areaData = [];
    	var outlierData = [];
    	var velocityData = [];
    	var mergerData = [];
    	const {width, height} = this.state;
    	const { data, nClusters, views, linkage } = this.props;
    	//FIXME!
    	const currentVar = 'mainMass';
    	const vel_cutoff = 4000;

    	if (typeof data !== "undefined" && '1' in data) {
    		const velocity_on = views.velocity_on,
    			  mergers_on = views.mergers_on,
    			  merger_demo_on = views.merger_demo_on
    		const dataStats = data[nClusters][linkage].dataStats;
    		//var { xRange, yRange } = this.state;
    		var xRange = {};
	    	const yRange = this.getYRange(dataStats, currentVar);
	    	const yRange_vel = this.getYRange(dataStats, 'mainVel', vel_cutoff);
	        for (var n = 0; n < nClusters; n++){
	            var array = dataStats[currentVar][n]; //FIXME
	            var velArray = dataStats['mainVel'][n]; //FIXME!
	            var mergerArray = dataStats['mergers'][n];
	            var clusterData = [];
	            var velocityRow = [];
	            var outlierRow = [];
	            var outlierRowVel = [];
	            xRange.min = 40; //FIXME!
	            xRange.max = Object.keys(array).length; //FIXME!
	            for (var t = 0; t < Object.keys(array).length; t++){
	                if (array[t].max != -Infinity && array[t].min != Infinity){
	                    var entry = {};
	                    var velEntry = {};
	                    var mergerEntryRow = [];
	                    entry.x = this.xScale(t, Object.keys(array).length-1);
	                    entry.y0 = height - (this.yScale(array[t].min, yRange) + this.clusterOffset(n));
	                    entry.y1 = height - (this.yScale(array[t].max, yRange) + this.clusterOffset(n));
	                    entry.q0 = height - (this.yScale(array[t].lower, yRange) + this.clusterOffset(n));
	                    entry.q1 = height - (this.yScale(array[t].upper, yRange) + this.clusterOffset(n));
	                    velEntry.x = this.xScale(t, Object.keys(array).length-1) + 0.5*width; //FIXME
	                    velEntry.y0 = height - (this.yScale(velArray[t].min, yRange_vel) + this.clusterOffset(n));
	                    velEntry.y1 = height - (this.yScale(velArray[t].max, yRange_vel) + this.clusterOffset(n));
	                    velEntry.q0 = height - (this.yScale(velArray[t].lower, yRange_vel) + this.clusterOffset(n));
	                    velEntry.q1 = height - (this.yScale(velArray[t].upper, yRange_vel) + this.clusterOffset(n));
	                    var x = this.xScale(t, Object.keys(array).length-1)
	                    for (var bin = 0; bin < Object.keys(mergerArray[t]).length; bin++){
	                    	const merger_width = 20; //FIXME
	                    	var mergerEntry = {}; 
	                    	mergerEntry.x0 = x;// + merger_width*mergerArray[t][bin].min;
	                    	mergerEntry.x1 = x + merger_width*mergerArray[t][bin].max;
	                    	mergerEntry.q0 = x + merger_width*mergerArray[t][bin].lower;
	                    	mergerEntry.q1 = x + merger_width*mergerArray[t][bin].upper;
	                   		mergerEntry.y = height - (this.yScale(mergerArray[t][bin].mass, yRange) + this.clusterOffset(n));
	                   		if (!isNaN(mergerEntry.x1)) mergerEntryRow.push(mergerEntry);
	                    }
	                    if (entry.y0 != entry.y1) { 
	                    	clusterData.push(entry);
	                    	velocityRow.push(velEntry);
	                    	if (mergerEntryRow.length > 0) mergerData.push(mergerEntryRow);

	                    	var outlierEntry = {};
	                    	var outlierEntryVel = {};
	                    	outlierEntry.x = this.xScale(t, Object.keys(array).length-1);
	                    	outlierEntry.y = height - (this.yScale(array[t].out, yRange) + this.clusterOffset(n));
	                    	outlierEntry.i = n;
	                    	outlierRow.push(outlierEntry); 
	                    	if (velocity_on == 1.0){
	                    		outlierEntryVel.x = this.xScale(t, Object.keys(array).length-1) + 0.5*width;
	                    		outlierEntryVel.y = height - (this.yScale(array[t].out_vel, yRange_vel) + this.clusterOffset(n));
	                    		outlierEntryVel.i = n;
	                    		outlierRowVel.push(outlierEntryVel);
	                    	}
	                    }
	                }
	            }
	            areaData.push(clusterData);
	            velocityData.push(velocityRow);
	            outlierData.push(outlierRow);
	            if (velocity_on == 1.0) { outlierData.push(outlierRowVel); }
	        }
	    	
	        const colors = defaultProps.colors;

            var _outerarea = d3.area()
            	.x( d => d.x )
            	.y0( d => d.y0 )
            	.y1( d => d.y1 );

            var _innerarea = d3.area()
            	.x( d => d.x )
            	.y0( d => d.q0 )
            	.y1( d => d.q1 );

            var _merger_outerarea = d3.area()
            	.x0( d => d.x0 )
            	.x1( d => d.x1 )
            	.y( d => d.y );

            var _merger_innerarea = d3.area()
            	.x0( d => d.q0 )
            	.x1( d => d.q1 )
            	.y( d => d.y );

            d3.select(node).selectAll('area outer mass').remove();
	        var outerArea = d3.select(node).selectAll('area outer mass')
	        	.data(areaData);

	        outerArea.exit().remove();
	        outerArea.enter().append('path')
	        	.style('fill', function(d,i){ return colors[(2*i)%colors.length]; })
	        	.style('opacity', 0.6)
	        	.attr('class', 'area outer mass')
	        	.attr('d', _outerarea);

	        var innerArea = d3.select(node).selectAll('area inner mass')
	        	.data(areaData);
	        innerArea.exit().remove();
	        innerArea.enter().append('path')
	        	.style('fill', function(d,i){ return colors[(2*i)%colors.length]; })
	        	.style('opacity', 1.0)
	        	.attr('class', 'area inner mass')
	        	.attr('d', _innerarea);

	        if (velocity_on == 1.0){
				var outerAreaVel = d3.select(node).selectAll('area outer vel')
					.data(velocityData);
				outerAreaVel.exit().remove();
				outerAreaVel.enter().append('path')
					.style('fill', function(d,i){ return colors[(2*i)%colors.length]; })
					.style('opacity', 0.6)
					.attr('class', 'area outer vel')
					.attr('d', _outerarea);

				var innerAreaVel = d3.select(node).selectAll('area inner vel')
					.data(velocityData);
				innerAreaVel.exit().remove();
				innerAreaVel.enter().append('path')
					.style('fill', function(d,i){ return colors[(2*i)%colors.length]; })
					.style('opacity', 1.0)
					.attr('class', 'area inner vel')
	            	.attr('d', _innerarea);  
	        } 	

	        if (mergers_on == 1.0){
	            var outerAreaMerger = d3.select(node).selectAll('area outer merger')
	            	.data(mergerData);
	            outerAreaMerger.exit().remove();
	            outerAreaMerger.enter().append('path')
	            	.style('fill', '#777777')
	            	.style('opacity', 0.6)
	            	.attr('class', 'area outer merger')
	            	.attr('d', _merger_outerarea);

	            var innerAreaMerger = d3.select(node).selectAll('area inner merger')
	            	.data(mergerData);
	            innerAreaMerger.exit().remove();
	            innerAreaMerger.enter().append('path')
	            	.style('fill', '#777777')
	            	.style('opacity', 1.0)
	            	.attr('class', 'area inner merger')
	            	.attr('d', _merger_innerarea);
	        }
	        var line = d3.line()
	            .x(function(d) { return d.x; })
	            .y(function(d) { return d.y; });
	        
	        var colors = this.colors;

	        this.drawAxes(xRange, yRange, yRange_vel);
	        
	        d3.select(node).selectAll(".line")
	            .data(outlierData)
	            .enter().append("path")
	            .style("stroke", function(d,i){ 
	            	var idx;
	            	if (velocity_on == 1.0){
	            		idx = i+1; 
	            		if (i%2 == 1) idx -= 1; 
	            	}
	            	else {
	            		idx = 2*i+1;
	            	}
	            	return colors[(idx)%colors.length]; })
	            .style("fill", "none")
	            .style("opacity", 1.0)
	            .style("stroke-width", "1px")
	            .attr("class", "line")
	            .attr("d", line);

	        //FIXME: put this somewhere else D:
	        if (merger_demo_on == 1.0){
		        const timeSeries = data[nClusters][linkage].timeSeries;
		        var xRange = {};
	            var clusterIDs = data[nClusters][linkage].clusterAssignments;
	            var mergerData = [];
	            xRange.min = 40; //FIXME!
	            xRange.max = Object.keys(dataStats[currentVar][0]).length; //FIXME!
	            for (var i = 0; i < timeSeries['mergerTimes'].length; i++){ //for each tree:
	            	const offset = this.clusterOffset(clusterIDs[i]);
	            	for (var t = 0; t < timeSeries['mergerTimes'][i].length; t++){ //for each merger entry:
	                	var entry = {};
	                	entry.x = this.xScale(timeSeries['mergerTimes'][i][t], xRange.max-1);
	                	entry.y = height - (this.yScale(timeSeries['mergedMasses'][i][t], yRange) + offset);
	                	entry.r = timeSeries['mergerRatios'][i][t]; //FIXME: custom scaling?
	                	if (entry.r > 0.05) mergerData.push(entry);
	                }
	            }
	            console.log("merger data: ", mergerData);
	            d3.select(node).selectAll('circle')
	            	.data(mergerData)
	            	.enter().append('circle')
	            	.style('fill', '#777777')
	            	.style('opacity', 0.7)
	            	.attr('r', function(d){ return d.r*18; })
	            	.attr('cx', function(d){ return d.x; })
	            	.attr('cy', function(d){ return d.y; });
	        }
	    }
    }

    drawTimeSeries(){
    	const node = this.node;
    	const currentVar = 'mainMass';
	    const { height } = this.state;
	    const { nClusters, views, data, linkage } = this.props;
	    const option = 'all'; //FIXME
	    if (typeof data !== "undefined" && views.main_branches_on == 1.0) {
	    	const clusterAssignments = data[nClusters][linkage].clusterAssignments;
	    	const dataStats = data[nClusters][linkage].dataStats;
	    	const main_branches_on = views.main_branches_on;

	    	const yRange = this.getYRange(dataStats, currentVar);
	        const mainMass = data[nClusters][linkage].timeSeries[currentVar];
		    var pathData = [];
		    var timeString = "month";
		    if (currentVar == 'mainMass') timeString = "mainTime";
		    
		    if (option == "none") { 
		        d3.select(node).selectAll(".line").remove();
		        return; 
		    }
		    
		    else if (option == "all"){
		        for (var i = 0; i < mainMass.length; i++){
		            var row = [];
	            	for (var t = 0; t < mainMass[i].length; t++){
		                var entry = {};
		                entry.x = this.xScale(data[nClusters][linkage].timeSeries[timeString][i][t], data[nClusters][linkage].timeSeries[timeString][i][data[nClusters][linkage].timeSeries[timeString][i].length-1]);
		                entry.y = height - (this.yScale(mainMass[i][t], yRange) + this.clusterOffset(clusterAssignments[i]));
		                row.push(entry);
		            }
		            pathData.push(row);
		        }
		    }
		    /*
		    else if (option == "random"){
		        var occurrences = {};
		        for (var i = 0; i < clusterAssignments.length; i++){
		            var idx = clusterAssignments[i];
		            if (idx in occurrences) { 
		                occurrences[idx].num += 1; 
		                occurrences[idx].indices.push(i);
		                }
		            else { 
		                occurrences[idx] = {};
		                occurrences[idx].num = 1; 
		                occurrences[idx].indices = [i];    
		            }
		        }
		        var min = timeSeries[this.currentVar].length;
		        for (var i = 0; i < this.nClusters; i++){
		            if (occurrences[i].num < min){ min = occurrences[i].num; }
		        }
		        console.log("size of smallest cluster: ", min);
		        var randomIndices = [];
		        for (var i = 0; i < this.nClusters; i++){
		            var count = 0;
		            var indices = occurrences[i].indices;
		            console.log("indices for this cluster: ", indices);
		            while (count < min){
		                element = indices[Math.floor(Math.random() * indices.length)];
		                indices.pop(element);
		                randomIndices.push(element);
		                count++;
		            }
		        }
		        for (var i = 0; i < randomIndices.length; i++){ //FIXME: can consolidate this into above
		            console.log("i: ", i);
		            var row = [];
		            for (t = 0; t < data[0].length; t++){
		                var idx = randomIndices[i];
		                entry = {};
		                entry.x = this.xScale(timeSeries[timeString][idx][t], timeSeries[timeString][idx][timeSeries[timeString][idx].length-1]);
		                entry.y = (this.height-this.xAxisHeight) - (this.yScale(data[idx][t], this.yRange) + this.clusterOffset(clusterAssignments[idx]));
		                row.push(entry);
		            }
		            pathData.push(row);
		        }
		    }
		    */
		        
		    var line = d3.line()
		        .x( d => d.x )
		        .y( d => d.y );
		    
		    const colors = defaultProps.colors;
		    
		    d3.select(node).selectAll(".line")
		        .data(pathData)
		        .enter().append("path")
		        .style("stroke", function(d,i){ 
		            if (option == 'all'){ return colors[(2*clusterAssignments[i]+1)%colors.length]; }
		            else if (option == 'random') { return colors[(2*clusterAssignments[randomIndices[i]]+1)%colors.length]; }})
		        .style("fill", "none")
		        .style("stroke-width", "1px")
		        .attr("class", "line")
		        .attr("d", line);

		var focus = d3.select(node).append("g")
			.attr("class", "focus")
			.style("display", "none");
			/*
		focus.on("click", function(d,i){
		    focus.attr("fill", "white");
		    focus.attr("opacity", 0.5);
		});
		*/

		focus.append("circle")
			.attr("r", 7)
			.attr("fill", '#777777')
			.attr("opacity", 0.8);

		focus.append("text")
			.attr("x", 15)
			.attr("dy", ".31em")
			.attr('font-family', 'sans-serif');

		d3.select(node).selectAll('.line')
			.on("mouseover", function() { focus.style("display", null); })
        	.on("mouseout", function() { 
        	    d3.select(this).style("stroke-width", "1px").style("stroke", function(d,i){
        	    	return colors[(2*clusterAssignments[i]+1)%colors.length]; 
        	   	});
        		focus.style("display", "none"); 
        	})
        	.on("mousemove", function(d,i){ 
        	    d3.select(this).style("stroke", 'black').style('stroke-width', '3px');
				mousemove(i);
		    })
		}

		function mousemove(i) {
      		var x0 = d3.mouse(node)[0];
      		var y0 = d3.mouse(node)[1];
  
      		focus.attr("transform", "translate(" + x0 + "," + y0 + ")");
      		focus.select("text").text("ID:" + i);
    	}
	}	

    xScale(x, max){
    	const { width } = this.state;
    	const { views } = this.props;
    	var w = width;
    	if (views.velocity_on == 1.0) {
    		w = width/2.;
    		return (x/max)*w;
    	}
        else return (x/max)*(0.95*w); //FIXME
    }
    yScale(y, yRange){
    	const { height, width } = this.state;
    	const { nClusters } = this.props;
        var frac = (y - yRange.min)/(yRange.max - yRange.min);
        if (y > yRange.max) frac = 1.0; 
        var clusterHeight = 0.9*(height)/nClusters;
        return frac*clusterHeight;
    }
    getYRange(dataStats, varName, cutoff){
    	var cutoff = cutoff || 0;
        var {maxes, mins } = this.state;
        const { nClusters } = this.props;
        for (var n = 0; n < nClusters; n++){
            var maxesForCluster = [];
            var minsForCluster = [];
            console.log("KEYS: ", Object.keys(dataStats));
            var length = Object.keys(dataStats[varName][n]).length;
            for (var i = 0; i < length; i++){ 
                maxesForCluster.push(dataStats[varName][n][i].max);
                minsForCluster.push(dataStats[varName][n][i].min);
            }
            var clusterMax = Math.max.apply(Math, maxesForCluster);
            var clusterMin = Math.min.apply(Math, minsForCluster);
            maxes.push(clusterMax);
            mins.push(clusterMin);
        }
        var range = {};
        range.min = Math.min.apply(Math, mins);
        range.max = Math.max.apply(Math, maxes);
        if (cutoff != 0) range.max = cutoff;
        return range;
    }
    clusterOffset(n){
    	const { height } = this.state;
    	const { nClusters } = this.props;
    	const heightPer = (height-120)/nClusters;
        return n*heightPer + 60;
    }

    drawAxes(xRange, yRange, yRange_vel){
    	const node = this.node;
		const { width, height } = this.state;
		const { nClusters, data, views } = this.props;
		var w = width;
		var velocity_on = views.velocity_on;
		if (velocity_on == 1.0) w = 0.5*width;
		console.log("velocity_on: ", velocity_on);
		if (typeof data !== "undefined" && '1' in data) {
			var xAxisScale = d3.scaleLinear()
				.domain([xRange.min, xRange.max])
				.range([60, w*0.95]);
					
			var yAxisHeight = 0.9*(height)/nClusters;

			var yAxisScale = d3.scaleLinear()
				.domain([yRange.min, yRange.max])
				.range([yAxisHeight,0]);

			if (yRange_vel){
				var yAxisScaleVel = d3.scaleLinear()
					.domain([yRange_vel.min, yRange_vel.max])
					.range([yAxisHeight,0]);
			}

			var xAxisBottom = d3.axisBottom()
				.scale(xAxisScale)
				.tickSize(4)
				.ticks(6);
			var xAxisTop = d3.axisTop()
				.scale(xAxisScale)
				.tickSize(4)
				.ticks(6);
			
			var yAxis = d3.axisLeft()
				.scale(yAxisScale)
				.tickSize(10)
				.ticks(4)
				.tickFormat(d3.format(".1e"));
			if (yRange_vel){
				var yAxisVel = d3.axisLeft()
					.scale(yAxisScaleVel)
					.tickSize(10)
					.ticks(4)
					.tickFormat(d3.format(".1e"));
			}

			var xGroup = d3.select(node).append("svg")
			    .attr("width", width)
			    .attr("height", height)
			    .attr("x", 0)
			    .attr("y", 0)
			    .attr("transform", function(){return "translate(" + "0" + "," + 0 + ")"; });
			    
			xGroup.append("g")
			    .attr("width", width)
			    .attr("height", 60)
			    .attr("transform", "translate(0,14)")
			    .call(xAxisTop);
			xGroup.append("g")
				.attr("width", width)
				.attr("height", 60)
				.attr("transform", function(){ return "translate(0," + (height-60) + ")"; })
				.call(xAxisBottom);

			if (velocity_on == 1.0){
				xGroup.append("g")
			    	.attr("width", width)
			    	.attr("height", 60)
			    	.attr("transform", function(){ return "translate(" + w + ",14)"; })
			    	.call(xAxisTop);
				xGroup.append("g")
					.attr("width", width)
					.attr("height", 60)
					.attr("transform", function(){ return "translate(" + w + "," + (height-60) + ")"; })
					.call(xAxisBottom);
			}
				  
		    xGroup.append("text")
				.text("timestep")
				.attr("transform", "translate(" + (width/2 + 20) + "," + (height-40) + ")")
				.attr("text-anchor", "middle")
				.attr("font-family", "sans-serif")
				.attr("font-size", "12px");
				  
			var yAxisData = [];
			var yAxisVelData = [];
			for (var i = 0; i < nClusters; i++){
				var yTransform = height - this.clusterOffset(i) - yAxisHeight;
				yAxisData.push(yTransform);
				yAxisVelData.push(yTransform);
			}	  
				  
			var yAxisContainer = d3.select(node).append("g")
				.attr("width", width)
				.attr("height", height);
			
			yAxisContainer.selectAll("svg")
				.data(yAxisData)
				.enter()
				.append("g")
				.attr("width", 60)
				.attr("height", height)
				.each(function(d,i){
					d3.select(this)
						.append("g")
						.attr("width", 60)
						.attr("height", height/nClusters)
						.attr("transform", function(d,i){ return "translate(" + 50 + "," + d + ")"; })
						.call(yAxis);
				});

			yAxisContainer.selectAll("svg")
				.data(yAxisVelData)
				.enter()
				.append("g")
				.attr("width", 60)
				.attr("height", height)
				.each(function(d,i){
					d3.select(this)
						.append("g")
						.attr("width", 60)
						.attr("height", height/nClusters)
						.attr("transform", function(d,i){ return "translate(" + (w+50) + "," + d + ")"; })
						.call(yAxisVel);
				});		
					
			
			yAxisContainer.append("text")
				.text("mass (kg)")
				.attr("transform", "translate(68," + height/2 + ")rotate(270)")
				.attr("text-anchor", "middle")
				.attr("font-family", "sans-serif")
				.attr("font-size", "12px");  
			if (velocity_on == 1.0){
				yAxisContainer.append("text")
				.text("velocity (m/s)")
				.attr("transform", "translate(" + (width/2 + 54) + "," + height/2 + ")rotate(270)")
				.attr("text-anchor", "middle")
				.attr("font-family", "sans-serif")
				.attr("font-size", "12px");  
			}
		}
	}

    render(){
    	const {width, height} = this.state;
        return <svg height={height} width={width} ref={node => this.node = node}>
        </svg>

        //FIXME: overhaul data access stuff (& make clustering dynamic)
        //var dataStats = calculateClusterRibbons(nClusters, clusterAssignments);
        //clusterView.drawClusters(dataStats, encodingOption);
        //clusterView.drawScatter(dataStats, timeSeries, clusterAssignments);
        //clusterView.drawTimeSeries(timeSeries, 1.0, clusterAssignments); //FIXME: fraction capability
    }
}

const mapStateToProps = state => ({
    data: state.items,
    loading: state.loading,
    error: state.error,
    nClusters: state.nClusters,
    linkage: state.linkage,
    views: state.views
});

const TreeViewContainer = connect(mapStateToProps)(TreeView);

export default TreeViewContainer;


// ClusterView.prototype.setNClusters = function(n){
//     this.nClusters = n;
//     this.dataContainer.selectAll("g").remove();
//     this.dataContainer.selectAll(".inner-border").remove();
//     var heightPer = (this.height - this.xAxisHeight)/n;
// 	for (i = 0; i < this.nClusters; i++){
// 		this.dataContainer.append("rect")
// 			.attr("class", "inner-border")
// 			.attr("width", this.width - this.yAxisWidth)
// 			.attr("height", heightPer+1)
// 			.attr("transform", "translate(" + this.yAxisWidth + "," + i*heightPer + ")");
// 	}
    
//     this.clusterGroup = this.dataContainer.append("g")
//     	.attr("height", this.height - this.xAxisHeight)
//     	.attr("width", this.width);
//     this.timeSeriesGroup = this.dataContainer.append("g")
//         .attr("height", this.height - this.xAxisHeight)
//     	.attr("width", this.width);
// }


//MOVE TO ANOTHER COMPONENT :D


//FIXME: do this not-on-deadline :)
// ClusterView.prototype.drawScatter = function(dataStats, timeSeries, clusterAssignments){
// 	this.yRange = this.getYRange(dataStats, 'mainVel');

// 	this.xRange.min = 0;
// 	this.xRange.max = 499; //FIXME!

//     var clusterIDs = clusterAssignments;
//     data = timeSeries['mainVel']; //this can be whatever
//     scatterData = [];
//     var timeString = "month";
//     if (this.currentVar == 'mainMass' || this.currentVar == 'mainVel') timeString = "mainTime";
    
//     //calculate random sample
//     var occurrences = {};
//     for (i = 0; i < clusterAssignments.length; i++){
//         var idx = clusterAssignments[i];
//         if (idx in occurrences) { 
//             occurrences[idx].num += 1; 
//             occurrences[idx].indices.push(i);
//             }
//         else { 
//             occurrences[idx] = {};
//             occurrences[idx].num = 1; 
//             occurrences[idx].indices = [i];    
//         }
//     }
//     var min = timeSeries[this.currentVar].length;
//     for (i = 0; i < this.nClusters; i++){
//         if (occurrences[i].num < min){ min = occurrences[i].num; }
//     }
//     console.log("size of smallest cluster: ", min);
//     var randomIndices = [];
//     for (i = 0; i < this.nClusters; i++){
//         var count = 0;
//         var indices = occurrences[i].indices;
//         while (count < min){
//             element = indices[Math.floor(Math.random() * indices.length)];
//             indices.pop(element);
//             randomIndices.push(element);
//             count++;
//         }
//     }
//     var longIndices = [];
//     for (i = 0; i < randomIndices.length; i++){ //FIXME: can consolidate this into above  
//         for (t = 0; t < data[0].length; t++){
//             var idx = randomIndices[i];
//             entry = {};
//             entry.x = this.xScale(timeSeries[timeString][idx][t], timeSeries[timeString][idx][timeSeries[timeString][idx].length-1]);        
//             //entry.y = (this.height-this.xAxisHeight) - (this.yScale(data[idx][t] - data[idx][data[0].length-1], this.yRange) + this.clusterOffset(clusterAssignments[idx]));
//             entry.y = (this.height-this.xAxisHeight) - (this.yScale(data[idx][t], this.yRange) + this.clusterOffset(clusterAssignments[idx]));
//             scatterData.push(entry);
//             longIndices.push(randomIndices[i]);
//         }
//     }
    
//     var colors = this.colors;
//     console.log("colors: ", colors);
    
//     this.timeSeriesGroup.selectAll(".circle")
//         .data(scatterData)
//         .enter().append("circle")
//         .style("fill", function(d,i){console.log("id: ", longIndices[i]); return colors[2*clusterIDs[longIndices[i]]+1]; })
//         .style("opacity", 0.7)
//         .attr("r", 4)
//         .attr("cx", function(d) { return d.x; })
//         .attr("cy", function(d) { return d.y; });
        
//     this.drawAxes();
// }
