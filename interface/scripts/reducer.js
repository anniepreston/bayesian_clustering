import {
	FETCH_DATA_BEGIN,
	FETCH_DATA_SUCCESS,
	FETCH_DATA_FAILURE,
  UPDATE_N_CLUSTERS,
  UPDATE_VIEW,
  UPDATE_MATCH_IDX,
  UPDATE_LINKAGE
} from './dataActions';


const initialState = {
	items: [],
  nClusters: 1,
	loading: false,
	error: null,
  linkage: 'complete',
  views: {
    velocity_on: 0.0,
    main_branches_on: 0.0,
    mergers_on: 0.0,
    merger_demo_on: 0.0
  },
  matchSettings: {
    idx: 0,
    normalized: 0
  }
};

export default function reducer(state = initialState, action){
	switch(action.type){
		case FETCH_DATA_BEGIN:
			return {
				...state,
				loading: true,
				error: null
			};
  	case FETCH_DATA_SUCCESS:
  		return {
  			...state,
  			loading: false,
  			items: action.payload.data,
  		};
  	case FETCH_DATA_FAILURE:
  		return {
  			...state,
    		loading: false,
    		error: action.payload.error,
    		items: {}
    	};
    case UPDATE_N_CLUSTERS:
      return {
        ...state,
        loading: false,
        nClusters: action.payload.n
      }
    case UPDATE_VIEW:
      return {
        ...state,
        views: {
          velocity_on: action.payload.velocity_on,
          main_branches_on: action.payload.main_branches_on,
          mergers_on: action.payload.mergers_on,
          merger_demo_on: action.payload.merger_demo_on
        }
      }
    case UPDATE_MATCH_IDX:
      return {
        ...state,
        matchSettings: {
          idx: action.payload.idx
        }
      }
    case UPDATE_LINKAGE:
      return {
        ...state,
        linkage: action.payload.linkage
      }
  	default:
  		return {
    		...state,
    		loading: false,
    		error: null
    	};
	}
}