//tree structure for dendrogram (hierarchy of clusters)

export function Node(data){
	this.data = data;
	this.left = null;
	this.right = null;
}

export default function ClusterTree(){
	this.root = null;
	this.nInputs = 0;
}

ClusterTree.prototype.getNodesAtLevel = function(node, level){
	var nodes = [node]
	var i = 0;
	while (i < level){
		var oldNodes = nodes;
		nodes = [];
		for (var j = 0; j < oldNodes.length; j++){
			if (oldNodes[j].left != null){
				nodes.push(oldNodes[j].left);
			}
			if (oldNodes[j].right != null){
				nodes.push(oldNodes[j].right);
			}
		}
		i++;
	}
	return nodes;
}

ClusterTree.prototype.insert = function(data, parent, level){
	var possibleLeaves = this.getNodesAtLevel(this.root, level);
	if (possibleLeaves.length > 0){
		for (var i = 0; i < possibleLeaves.length; i++){
			if (possibleLeaves[i].data == parent){
				var dataMax = Math.max(data.a, data.b);
				var dataMin = Math.min(data.a, data.b);
				possibleLeaves[i].right = new Node(dataMax);
				possibleLeaves[i].left  = new Node(dataMin);
			}
			if (possibleLeaves[i].data != parent 
				&& possibleLeaves[i].left == null
				&& possibleLeaves[i].right == null){
			
				possibleLeaves[i].right = new Node(possibleLeaves[i].data);
			}
		}
	}
}

ClusterTree.prototype.getClusterAssignments = function(nClusters){
	var nodes = this.getClusterRoots(nClusters);
	var clusterAssignments = {};
	var clusterID = 0;
	
	for (var i = 0; i < nodes.length; i++){
		var returnList = [];
		var nInputs = this.nInputs;
		getAllLeaves(returnList, nodes[i], nInputs);
		for (var j = 0; j < returnList.length; j++){
			clusterAssignments[returnList[j].data] = clusterID;
		}
		clusterID++;
	}
	
	function getAllLeaves(returnList, node, nInputs){
		if (node.left == null && node.right == null){
			if (node.data < nInputs) { returnList.push(node) };
		}
		if (node.left != null) { getAllLeaves(returnList, node.left, nInputs); }
		if (node.right != null) { getAllLeaves(returnList, node.right, nInputs); }
	}
	
	return Object.keys(clusterAssignments).map(function (key) { return clusterAssignments[key]; });
};

ClusterTree.prototype.getClusterRoots = function(nClusters){
    if (nClusters == 1) return [this.root]; //FIXME

	var nodes = [];
	var oldNodes  = [];
	nodes.push(this.root);
	var i = 0;
	while (i < nClusters){
		oldNodes = nodes;
		nodes = [];
		for (var j = 0; j < oldNodes.length; j++){
			if (oldNodes[j].left != null) nodes.push(oldNodes[j].left);
			if (oldNodes[j].right != null) nodes.push(oldNodes[j].right);
		}
		i = nodes.length;
	}
	return nodes;
}
