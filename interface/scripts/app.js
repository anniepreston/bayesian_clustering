import thunkMiddleware from 'redux-thunk';
import logger from 'redux-logger';
import TreeViewContainer from './treeView';
import { createStore, applyMiddleware } from 'redux';
import LayoutWrapper from './LayoutWrapper';
import reducer from './reducer';
import { Provider } from 'react-redux';
import {render} from 'react-dom';

import '../styles/main.css'; //this is important

const store = createStore(reducer, applyMiddleware(thunkMiddleware, logger));

render(React.createElement(
	Provider,
	{ store: store },
	React.createElement(LayoutWrapper, null)
), document.body.appendChild(document.getElementById('root')));